import mongoose from 'mongoose'

const Schema = mongoose.Schema;

let Conversation = new Schema({    
    messages : {
        type: Array
    }
})

export default mongoose.model('Conversation', Conversation, 'conversations');