import mongoose from 'mongoose'

const Schema = mongoose.Schema;

let Deal = new Schema({
    name : {
        type: String
    },
    country : {
        type: String
    },
    city : {
        type: String
    },
    address : {
        type: String
    },
    type : {
        type: String
    },
    stores : {
        type: String
    },
    size : {
        type: Number
    },
    rooms : {
        type: Number
    },
    furniture : {
        type: Boolean
    },
    video : {
        type: Object
    },
    sale : {
        type: Boolean
    },
    price : {
        type: Number
    },
    owner : {
        type: String
    },
    approved : {
        type: Boolean
    },
    dateTaken : {
        type: Array
    }
})

export default mongoose.model('Deal', Deal, 'deals');