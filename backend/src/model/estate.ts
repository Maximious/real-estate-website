import mongoose from 'mongoose'

const Schema = mongoose.Schema;

let Estate = new Schema({
    name : {
        type: String
    },
    country : {
        type: String
    },
    city : {
        type: String
    },
    address : {
        type: String
    },
    type : {
        type: String
    },
    stores : {
        type: String
    },
    size : {
        type: Number
    },
    rooms : {
        type: Number
    },
    furniture : {
        type: Boolean
    },
    video : {
        type: Object
    },
    sale : {
        type: Boolean
    },
    price : {
        type: Number
    },
    owner : {
        type: String
    },
    approved : {
        type: Boolean
    },
    promoted : {
        type: Boolean
    }
})

export default mongoose.model('Estate', Estate, 'estates');