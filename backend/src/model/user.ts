import mongoose from 'mongoose'

const Schema = mongoose.Schema;

let User = new Schema({
    username: {
        type: String
    },
    password: {
        type: String
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    type: {
        type: String
    },
    mail: {
        type: String
    },
    country: {
        type: String
    },
    city: {
        type: String
    },
    status: {
        type: String
    },
    img : {
        type: String
    },
    blockedUsers : {
        type : Array
    },
    inbox : {
        type : Array
    }
})

export default mongoose.model('User', User, 'users');