import express from 'express';
import cors from 'cors'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import user from './model/user'
import estate from './model/estate'
import conversation from './model/conversation';

const app = express();

app.use(cors())
app.use(bodyParser.json({limit: '10mb'}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))

mongoose.connect("mongodb://localhost:27017/nekretnine"); 

const conn = mongoose.connection;
const ObjectID = require('mongodb').ObjectID;

conn.once('open',()=>{
    console.log('Connection successfull');
});

const router = express.Router();

router.route('/login').post((req, res)=>{
    let username = req.body.username;
    let password = req.body.password;

    user.findOne({"username":username, "password":password, "status":'approved'}, (err, user)=>{
        if(err) 
        {
            console.log(err);
        }
        else
        {            
            res.json(user);
        }           
    })
});

router.route('/register').post((req, res)=>{
    /*
    const data={
        username: req.body.username,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mail: req.body.mail,
        country: req.body.country,
        city: req.body.city,
        img: req.body.img
      }

      console.log(data);
      */

    req.body.blockedUsers = [];
    req.body.inbox = [];
    let u = new user(req.body);
    //console.log(u);
        
    u.save().then(u=>{
        res.status(200).json(true);
    }).catch(err=>{
        res.status(400).json(false);        
    })
});

router.route('/changeSettings').post((req, res)=>{
    let id = req.body._id;   
    
    user.collection.updateOne({'_id':new ObjectID(id)},{ 
        $set: { 
            username: req.body.username,
            password: req.body.password,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            mail: req.body.mail,
            country: req.body.country,
            city: req.body.city,
            img: req.body.img
        }}, (err, user)=>{
            if(err) 
            {
                console.log(err);
                res.status(400).json(false);
            }
            else
            {            
                res.status(200).json(true);
            }        
        }
    )
});

router.route('/checkUsernameTaken').post((req, res)=>{
    let username = req.body.username;    

    user.findOne({"username":username}, (err, user)=>{        
        if(err) 
        {
            console.log(err);
        }
        else
        {           
            if( user == null )
            {                
                res.json(true);
            }                
            else
            {
                res.json(false);
            }         
        }           
    })
});

router.route('/checkMailTaken').post((req, res)=>{
    let mail = req.body.mail;

    user.findOne({"mail":mail}, (err, user)=>{        
        if(err) 
        {
            console.log(err);
        }
        else
        {           
            if( user == null )
            {                
                res.json(true);
            }                
            else
            {
                res.json(false);
            }         
        }           
    })
});

router.route('/findByUsername').get((req, res)=>{
    let username = req.query.username;

    user.findOne({'username' : username}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/approveUser').get((req, res)=>{
    let username = req.query.username;

    user.collection.updateOne({'username' : username},
        { $set : { 'status' : 'approved' } }, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/removeUser').get((req, res)=>{
    let username = req.query.username;

    user.collection.deleteOne({'username' : username}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/getEstateById').post((req, res)=>{
    let id = req.body._id;

    estate.collection.findOne({ '_id' : new ObjectID(id) }, (err, estate)=>{                
        if(err) 
        {
            console.log(err);
        }
        else
        {                      
            res.json(estate);
        }           
    })
});

router.route('/users').get((req, res)=>{
    user.find({'type': { $ne : 'admin' }}, (err, users)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(users);
        }
    })
});

router.route('/approvedUsers').get((req, res)=>{
    user.find({'type': { $ne : 'admin' }, 'status' : 'approved'}, (err, users)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            //console.log(users);
            res.json(users);
        }
    })
});

router.route('/waitingUsers').get((req, res)=>{
    user.find({'type': { $ne : 'admin' }, 'status' : 'waiting'}, (err, users)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {            
            //console.log(users);
            res.json(users);
        }
    })
});

router.route('/search').post((req, res)=>{
    let city = req.body.city;
    if(city == null || city == undefined)
    {
        city=".*";
    }        
    let range = {$gt : req.body.lPrice, $lt : req.body.hPrice};
    if(req.body.lPrice == null || req.body.lPrice == undefined)
    {
        range['$gt'] = -1;
    }
    if(req.body.hPrice == null || req.body.hPrice == undefined)
    {
        delete range['$lt'];
    }        
    estate.find({'approved':true, "city": { "$regex": city, "$options": "i" }, 
        "price" : range }, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/getMyEstates').post((req, res)=>{
    let username = req.body.username;
            
    estate.find({'owner' : username}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/newEstate').post((req, res)=>{
    let fullAdress =  req.body.region + ", " 
        + req.body.address + ", " 
        + req.body.number;  
    
    req.body.address = fullAdress;
    req.body.video = {
        img : [],
        video : []
    };
    req.body.approved = false;
    req.body.promoted = false;
    let e = new estate(req.body);
    //console.log(e);
        
    e.save().then(e=>{        
        res.status(200).json(e._id);
    }).catch(err=>{        
        res.status(400).json(false);        
    })
});

router.route('/updateEstate').post((req, res)=>{
    let id = req.body._id;   
    let fullAdress =  req.body.region + ", " 
        + req.body.address + ", " 
        + req.body.number;  
    
    estate.collection.updateOne({'_id':new ObjectID(id)},{ 
        $set: { 
            name : req.body.name,
            country : req.body.country,
            city : req.body.city,
            address : fullAdress,
            type : req.body.type,
            stores : req.body.stores,
            size : req.body.size,
            rooms : req.body.rooms,
            furniture : req.body.furniture,
            price : req.body.price,
            sale : req.body.sale,
        }}, (err, user)=>{
            if(err) 
            {
                console.log(err);
                res.status(400).json(false);
            }
            else
            {            
                res.status(200).json(true);
            }        
        }
    )
});

router.route('/addEstateImg').post((req, res)=>{
    estate.collection.updateOne({'_id':new ObjectID(req.body.id)}, 
        { $push: { 'video.img': req.body.img } }, (err, estate)=>{
            if(err) 
            {
                console.log(err);
                res.status(400).json(false);
            }
            else
            {                            
                res.status(200).json(true);
            }        
        }
    )
});

router.route('/removeEstateImg').post((req, res)=>{         
    estate.collection.updateOne({'_id':new ObjectID(req.body.id)}, 
    {$pull : {"video.img" : req.body.img}}).then(i=>{
        res.status(200).json(true);
    })            
});

router.route('/estates').get((req, res)=>{
    estate.find({}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/estatesPromoted').get((req, res)=>{
    estate.find({'promoted' : true}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/estatesApproved').get((req, res)=>{
    estate.find({'approved' : true}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/estatesUnapproved').get((req, res)=>{
    estate.find({'approved' : false}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/approveEstate').post((req, res)=>{
    let id = req.body._id;       
    
    estate.collection.updateOne({'_id':new ObjectID(id)},{ 
        $set: { 
            approved : true
        }}, (err, user)=>{
            if(err) 
            {
                console.log(err);
                res.status(400).json(false);
            }
            else
            {            
                res.status(200).json(true);
            }        
        }
    )
});

router.route('/setPromotedEstate').post((req, res)=>{
    let id = req.body._id;       
    let promoted = req.body.promoted;
    
    estate.collection.updateOne({'_id':new ObjectID(id)},{ 
        $set: { 
            promoted : promoted
        }}, (err, user)=>{
            if(err) 
            {
                console.log(err);
                res.status(400).json(false);
            }
            else
            {            
                res.status(200).json(true);
            }        
        }
    )
});

router.route('/deleteEstate').post((req, res)=>{
    let id = req.body._id;

    estate.collection.deleteOne({'_id':new ObjectID(id)}, (err, estates)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});  

router.route('/statisticEstatePriceRange').get((req, res)=>{
    estate.aggregate([
        { "$match": { "approved": true }},
        { "$group": { "_id": {
                "$cond": [
                    { "$lt": [ "$price", 100 ] }, "<100",
                    { "$cond": [{ "$lt": [ "$price", 200 ] }, "100-200", 
                    { "$cond": [{ "$lt": [ "$price", 300 ] }, "200-300", 
                    { "$cond": [{ "$lt": [ "$price", 1000 ] }, "300-1000",
                    { "$cond": [{ "$lt": [ "$price", 40000 ] }, "<40000",
                    { "$cond": [{ "$lt": [ "$price", 50000 ] }, "40000-50000",
                    { "$cond": [{ "$lt": [ "$price", 60000 ] }, "50000-60000",
                    { "$cond": [{ "$lt": [ "$price", 1000 ] }, "<100000",
                    { "$cond": [{ "$lt": [ "$price", 110000 ] }, "100000-110000",
                    { "$cond": [{ "$lt": [ "$price", 120000 ] }, "110000-120000", "120000+"]}]}]}]}]}]}]}]}]}                            
                    ]
                },
        "count": { "$sum": 1 }
    }}], (err: any, estates: any)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates);
        }
    })
});

router.route('/statisticEstateCityCount').get((req, res)=>{
    estate.aggregate([{"$match" : { approved : true }},
        {"$group" : { _id: "$city", count:{$sum:1}}}], 
        (err: any, estates: any)=>{
            if(err)
            {
                console.log(err);
            }
            else
            {
                res.json(estates);
            }
    })
});

router.route('/statisticEstateTypeCount').get((req, res)=>{
    estate.aggregate([{"$match" : { approved : true }},
        {"$group" : {_id: { type : "$type", sale : "$sale" }, count:{$sum:1}}}], 
        (err: any, estates: any)=>{
            if(err)
            {
                console.log(err);
            }
            else
            {
                res.json(estates);
            }
    })
});

router.route('/getConversation').post((req, res)=>{
    let id = req.body._id;   
    
    conversation.findById(id, (err, user)=>{
            if(err) 
            {
                console.log(err);
                res.status(400).json(false);
            }
            else
            {            
                res.status(200).json(true);
            }        
        }
    )
});

router.route('/activeMessages').post((req, res)=>{
    let username = req.body.username;    

    user.aggregate([ 
            { $match : { 'username' : username } },
            { $unwind: { path: "$inbox" } },
            { $project: { _id : 0, "inbox": 1 } },
            { $sort : { 'inbox.lastMessageDate' : -1 } },
            { $replaceRoot : { "newRoot" : "$inbox" }}
        ], (err : any, estates : any)=>{
            if(err)
            {
                console.log(err);
            }
            else
            {
                res.json(estates);
            }
    })
});

router.route('/getConversationById').post((req, res)=>{
    let _id = req.body._id;   

    conversation.findById(_id, (err : any, estates : any)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(estates.messages);
        }
    })
});

router.route('/sendMessage').post((req, res)=>{
    let _id = req.body._id;
    let existingConv = req.body.existingConv;
    let sender = req.body.sender;   
    let receiver = req.body.receiver;
    let text = req.body.text;   
    let date = req.body.date;

    let data = {        
        "text" : text,
        "date" : date,
        "sender" : sender      
    }

    if(existingConv) {
        conversation.collection.updateOne(
            {_id:new ObjectID(_id)},
            { $push : { messages : data } }, (err, user)=>{
                if(err) 
                {
                    console.log(err);
                    res.status(400).json(false);
                }
                else
                {            
                    res.status(200).json(true);
                }        
            }
        )
    } else {
        //continue
    }     
});

router.route('/getUsersBlockingMe').post((req, res)=>{
    let username = req.body.username;
    
    user.aggregate([
        { $unwind: { path: "$blockedUsers" } },
        { $match : { "blockedUsers" : username }},
        { $project: { "username": 1 } }       
    ], (err : any, users : any)=>{
        if(err)
        {
            console.log(err);
        }
        else
        {    
            res.json(users);
        }
    })
});

router.route('/createNewConversation').post((req, res)=>{
    let username = req.body.username;
    let lastMessageDate = new Date();
    let title = req.body.title;
    let comUser = req.body.comUser;        
    let text = req.body.text;
    let newConvId;
        
    conversation.collection.insertOne({
        messages : {
            text : text,
            date : lastMessageDate,
            sender : username
        }
    }, ((err, id)=> {
        //console.log(id.insertedId);
        newConvId = id.insertedId;

        user.collection.updateOne({"username" : username},{ 
            $push: { "inbox" : {
                "lastMessageDate" : lastMessageDate,
                "title" : title,
                "archived" : false,
                "comUser" : comUser,
                "offer" : false,
                "offerPrice" : 0,
                "owner" : true,
                "idConv" : newConvId
            }     
            }}, (err, user)=>{
                if(err) 
                {
                    console.log(err);
                    res.status(400).json(false);
                }              
            }
        )    
    
        user.collection.updateOne({"username" : comUser},{ 
            $push: { "inbox" : {
                "lastMessageDate" : lastMessageDate,
                "title" : title,
                "archived" : false,
                "comUser" : username,
                "offer" : false,
                "offerPrice" : 0,
                "owner" : true,
                "idConv" : newConvId
            }     
            }}, (err, user)=>{
                if(err) 
                {
                    console.log(err);
                    res.status(400).json(false);
                }
                else
                {            
                    res.status(200).json(true);
                }        
            }
        )
    }))    
});

router.route('/archiveMessage').post((req, res)=>{
    let username = req.body.username;
    let idConv = req.body.idConv;

    let newData : any;
    
    user.aggregate([
        { $match : { "username" : username} },
        { $unwind: { path: "$inbox" } },
        { $match : { "inbox.idConv" : idConv} }
    ], (err: any, conversations: any)=>{
        if(err)
        {
            console.log(err);
        }        
        else
        {
            newData = conversations[0].inbox;
            //res.json(conversations);
        }

        newData.archived = !newData.archived;

        user.update(
            { username: username },
            { $pull: { 'inbox': { idConv: idConv } } },
                (err: any, conversations: any)=>{
                    user.update(
                        { username: username },                
                        { $push: { 'inbox': newData } },
                            (err : any, answer : any) =>{
                                res.json(true);
                    });      
            });                               
    });
});

router.route('/blockUser').post((req, res)=>{
    let username = req.body.username;
    let blockingUser = req.body.blockingUser;    
    
    user.update(
        { username: username },
        { $push: { 'blockedUsers': blockingUser } },
            (err: any, conversations: any)=>{
                res.json(true);
        });
});

router.route('/unblockUser').post((req, res)=>{
    let username = req.body.username;
    let blockingUser = req.body.blockingUser;    
    
    user.updateOne(
        { username: username },
        { $pull: { 'blockedUsers': blockingUser } },
            (err: any, conversations: any)=>{
                res.json(true);
        });
});

router.route('/setOffer').post((req, res)=>{
    let username = req.body.username;
    let idConv = req.body.idConv;
    let comUser = req.body.comUser;
    let offerPrice = req.body.offerPrice;
    let newData : any;
    
    user.aggregate([
        { $match : { "username" : username} },
        { $unwind: { path: "$inbox" } },
        { $match : { "inbox.idConv" : idConv} }
    ], (err: any, conversations: any)=>{
        if(err)
        {
            console.log(err);
        }        
        else
        {
            newData = conversations[0].inbox;
            //res.json(conversations);            
        }

        newData.offer = true;
        newData.offerPrice = offerPrice;
        console.log(newData);


        user.updateOne(
            { username: username },
            { $pull: { 'inbox': { idConv: idConv } } },
                (err: any, conversations: any)=>{
                    user.update(
                        { username: username },                
                        { $push: { 'inbox': newData } },
                            (err : any, answer : any) =>{
                            if(err)  console.log(err);

                            if(comUser == 'agency')
                            {
                                user.updateOne(
                                    { username: comUser },
                                    { $pull: { 'inbox': { idConv: idConv } } },
                                        (err: any, conversations: any)=>{
                                            user.update(
                                                { username: comUser },                
                                                { $push: { 'inbox': newData } },
                                                    (err : any, answer : any) =>{
                                                        res.json(true);
                                            });      
                                    });              
                            }
                            else
                            {
                                user.updateMany(
                                    { type: 'worker' },
                                    { $pull: { 'inbox': { idConv: idConv } } },
                                        (err: any, conversations: any)=>{
                                            user.update(
                                                { username: comUser },                
                                                { $push: { 'inbox': newData } },
                                                    (err : any, answer : any) =>{
                                                        res.json(true);
                                            });      
                                    });              
                            }                                             
                    });                          
            });                               
        
    });
});

router.route('/removeOffer').post((req, res)=>{
    let username = req.body.username;
    let comUser = req.body.comUser;
    let idConv = req.body.idConv;    
    let isWorker = req.body.isWOrker;
    let newData : any;    
    
    user.aggregate([
        { $match : { "username" : username} },
        { $unwind: { path: "$inbox" } },
        { $match : { "inbox.idConv" : idConv} }
    ], (err: any, conversations: any)=>{
        if(err)
        {
            console.log(err);
        }        
        else
        {
            newData = conversations[0].inbox;
            //res.json(conversations);
        }

        newData.offer = false;
        newData.offerPrice = 0;
        console.log(newData);

        user.updateOne(
            { username: username },
            { $pull: { 'inbox': { idConv: idConv } } },
                (err: any, conversations: any)=>{
                    user.update(
                        { username: username },                
                        { $push: { 'inbox': newData } },
                            (err : any, answer : any) =>{
                            if(err)  console.log(err);

                            if(isWorker)
                            {
                                user.updateMany(
                                    { type: 'worker' },
                                    { $pull: { 'inbox': { idConv: idConv } } },
                                        (err: any, conversations: any)=>{
                                            user.update(
                                                { username: comUser },                
                                                { $push: { 'inbox': newData } },
                                                    (err : any, answer : any) =>{
                                                        res.json(true);
                                            });      
                                    });                               
                            }
                            else
                            {
                                user.updateOne(
                                    { username: comUser },
                                    { $pull: { 'inbox': { idConv: idConv } } },
                                        (err: any, conversations: any)=>{
                                            user.update(
                                                { username: comUser },                
                                                { $push: { 'inbox': newData } },
                                                    (err : any, answer : any) =>{
                                                        res.json(true);
                                            });      
                                    });                               
                            }
                    });      
            });                                       
    });
});

app.use('/', router);
app.listen(4000, () => console.log(`Express server running on port 4000`));