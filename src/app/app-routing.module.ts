import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { CreateEstateComponent } from './create-estate/create-estate.component';
import { EstateListComponent } from './estate-list/estate-list.component';
import { EstatePageComponent } from './estate-page/estate-page.component';
import { EstateSettingsComponent } from './estate-settings/estate-settings.component';
import { HomeComponent } from './home/home.component'
import { InboxComponent } from './inbox/inbox.component';
import { LogoutComponent } from './logout/logout.component';
import { MyEstatesComponent } from './my-estates/my-estates.component';
import { SettingsComponent } from './settings/settings.component';
import { UserListComponent } from './user-list/user-list.component';
import { WorkerComponent } from './worker/worker.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "estate", component: EstatePageComponent},
  {path: "estate/settings", component: EstateSettingsComponent},
  {path: "myEstates", component: MyEstatesComponent},  
  {path: "newEstate", component: CreateEstateComponent},  
  {path: "admin", component: AdminComponent},  
  {path: "admin/users", component: UserListComponent},  
  {path: "admin/estates", component: EstateListComponent},  
  {path: "worker", component: WorkerComponent},
  {path: "worker/estates", component: EstateListComponent},  
  {path: "user", component: HomeComponent},
  {path: "settings", component: SettingsComponent},  
  {path: "inbox", component: InboxComponent},  
  {path: "logout", component: LogoutComponent},  
  {path: '**', component: HomeComponent } //default route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
