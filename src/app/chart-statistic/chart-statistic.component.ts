import { Component, Input, OnInit } from '@angular/core';
import { Chart, ChartTypeRegistry, registerables } from 'chart.js';
import { EstateService } from '../estate.service';
Chart.register(...registerables);

@Component({
  selector: 'chart-statistic',
  templateUrl: './chart-statistic.component.html',
  styleUrls: ['./chart-statistic.component.css']
})
export class ChartStatisticComponent implements OnInit {

  @Input() state : number = 0;
  chartId : string;

  constructor(
      private estateService : EstateService
  ) { }

  ngOnInit(): void {
    let data : Array<number> = [];
    let label : string = 'Broj nekretnina';
    let labels : Array<string> = [];
    let chartType;

    this.chartId = "chart" + this.state;

    if(this.state == 0) {
        this.estateService.statisticEstateCityCount().subscribe(answer => {   
            console.log(answer);

            for( let i in answer ) {
                data.push(answer[i].count);
                labels.push(answer[i]._id);
            }                                   
            chartType = 'bar';                  
            this.formGraph(label, labels, data, chartType);
        })
    }
    else if(this.state == 1) {
        this.estateService.statisticEstateTypeCount().subscribe(answer => {        
            let data1, data2, data3, data4;
            for( let i in answer ) {            
                if(answer[i]['_id']['sale']) {
                    if(answer[i]['_id']['type'] == 'stan') {
                        data2 = answer[i]['count'];
                    } else {
                        data4 = answer[i]['count'];
                    }
                } else {
                    if(answer[i]['_id']['type'] == 'stan') {
                        data1 = answer[i]['count'];
                    } else {
                        data3 = answer[i]['count'];
                    }
                }
            }
            data = [data1, data2, data3, data4];
            labels = ['Stan (iznajmljivanje)', 'Stan (prodaja)', 'Kuća (iznajmljivanje)', 'Kuća (prodaja)'];          
            
            chartType = 'line';
            this.formGraph(label, labels, data, chartType);
        })
    }
    else {        
        this.estateService.statisticEstatePriceRange().subscribe(answer => {
            console.log(answer);

            for( let i in answer ) {
                data.push(answer[i].count);
                labels.push(answer[i]._id);
            }                                     
            chartType = 'doughnut';          
            this.formGraph(label, labels, data, chartType);
        })
    }
  }

  formGraph(label : string, labels : Array<string>, data : Array<number>, chartType) {     
    let myChart = new Chart(this.chartId, {
        type: chartType,
        data: {
            labels: labels,
            datasets: [{
                label: label,
                data: data,
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',                        
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',                        
                ],
                borderWidth: 1
            }]
        },
        options: {            
            plugins: {
                textStrokeColor: {
                  color: 'white'
                }
              }
            ,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });     
  }

}
