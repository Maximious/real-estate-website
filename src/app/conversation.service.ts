import { _DisposeViewRepeaterStrategy } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  uri = 'http://localhost:4000';

  constructor(private http: HttpClient) { }

  getConversationById(id){
    const data={
      _id : id      
    }
    return this.http.post(`${this.uri}/getConversationById`, data);
  }  

  sendMessage(id, existingConv, sender, receiver, text) {    
    const data={
      _id : id,
      existingConv : existingConv,
      sender : sender,
      receiver : receiver,
      text : text,
      date : new Date()
    }
        
    return this.http.post(`${this.uri}/sendMessage`, data);
  }     

  /*
  findByUsername(username){
    return this.http.get(`${this.uri}/findByUsername?username=${username}`);
  }
  */
}
