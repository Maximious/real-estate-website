import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NumberValueAccessor, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidator } from '../custom-validator';
import { EstateService } from '../estate.service';

@Component({
  selector: 'app-create-estate',
  templateUrl: './create-estate.component.html',
  styleUrls: ['./create-estate.component.css']
})
export class CreateEstateComponent implements OnInit {

  estateForm: FormGroup;
  loading = false;
  submitted = false;
  filesSent : number = 0;
  adminOrWorkerLogged : boolean = false;
  owner : string;
  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private estateService : EstateService
  ) { }

  ngOnInit() {        
    let user = JSON.parse(localStorage.getItem('loggedIn'));
    if(user == undefined) {
      this.router.navigate['/'];
    }
    else {
      if( user.type != 'user' ) {
        this.adminOrWorkerLogged = true;
        this.owner = 'agency';        
      } else {
        this.owner = user.username;
      }
    }

    this.estateForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.maxLength(28)]],
        country: ['', [Validators.required]],
        city: ['', [Validators.required]],
        region: ['', [Validators.required]],
        adress: ['', [Validators.required]],
        number: ['', [Validators.required]],
        type : [false, []],
        stores : ['', [Validators.required]],
        size: ['', Validators.required],
        rooms: ['', Validators.required],
        furniture: [false, []],        
        price: ['', [Validators.required]],
        sale: [false,[]],
        owner: [this.owner,[]],
        img: ['',[]],
        approved: [this.adminOrWorkerLogged,[]]
    });            
  }

  //getters
  get f() { return this.estateForm.controls; }  
  get fe() { return this.estateForm.errors; }

  onSubmit() {
    this.submitted = true;        

    // stop here if form is invalid
    console.log(this.f);
    if (this.estateForm.invalid) {                
        this.loading = false;
        return;
    }
        
    if( this.f.type.value == true || this.f.type.value == 'kuća' ) {
      this.f.type.setValue('kuća');
    }
    else {
      this.f.type.setValue('stan');
    }

    this.loading = true;
    
    this.estateService.createNew(this.estateForm)
            .subscribe((answer)=>{
              let id = answer;
              let numOfPhotos;
              if(this.f.img.value == "") {
                this.router.navigate(['/myEstates']);                
              } else {                
                numOfPhotos = this.f.img.value.length;
              }
              while(this.filesSent < numOfPhotos)
              {                                                
                let fileReader = new FileReader();                
                let imageLocation = this.f.img.value[this.filesSent];
                this.filesSent++;

                fileReader.onload = (e) => {           
                  let img = fileReader.result as string;                        

                  this.estateService.addEstateImg(id, img).subscribe((answer)=>{
                    if( this.filesSent >= this.f.img.value.length ) {
                      this.router.navigate([
                        'myEstates'
                      ]);                      
                    } 
                  })                         
                }
                fileReader.readAsDataURL(imageLocation);
              }              
          })    
  }

  //imgUpload
  handleFileInput(files: FileList) {
    this.f.img.setValue(files);    
  }
}
