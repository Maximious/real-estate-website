import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { UserService } from "./user.service";

@Injectable({
  providedIn: 'root' 
})
export class CustomValidator {

  constructor(        
      private userService : UserService
    ) { }

  areEqual() {    
    return (group: FormGroup): {[key: string]: any} => {                                     
    
      let password = group.value.password;
      let confirmPassword = group.value.repeatPassword;          
      
      if (password !== confirmPassword) {
        return {
          mismatchedPasswords: true
        };
      }

      return null;
    }
  }

  //three consequent characters used
  threeConsequent() {
    return (group: FormGroup): {[key: string]: any} => {            
      let regex = /((.)\2{2})/g;       
  
      if (regex.test(group.value))
      {        
        return {
          consequentCharacters: true
        };
      }
      else
      {
        return null;        
      }        
    }
  }

  uniqueUsername() {
    return (group: FormGroup): { [key: string]: any; } => {                          

       this.userService.checkUniqueUsername(group.value)
        .subscribe((answer: boolean)=>{           
            if(answer == false){                                
                const newError = 
                {
                  unique: 
                  {
                    message: "Username already taken",
                  }
                }
                group.setErrors(newError);                    
            }
      }); 
      
      return null;
    }    
  }
  
  existingUser() {
    return (group: FormGroup): { [key: string]: any; } => {                          

       this.userService.checkUniqueUsername(group.value)
        .subscribe((answer: boolean)=>{           
            if(answer == true){                                
                const newError = 
                {
                  nonExisting: 
                  {
                    message: "Unknown user",
                  }
                }
                group.setErrors(newError);                    
            }
      }); 
      
      return null;
    }    
  }

  uniqueMail() {
    return (group: FormGroup): { [key: string]: any; } => {                          
       
       this.userService.checkUniqueMail(group.value)
        .subscribe((answer: boolean)=>{            
            if(answer == false){                                
                const newError = 
                {
                  unique: 
                  {
                    message: "Mail already taken",
                  }
                }
                group.setErrors(newError);                    
            }
      }); 
      
      return null;
    }    
  } 

  passwordChange() {    
    return (group: FormGroup): {[key: string]: any} => {                                     
          
      let oldPassword = group.value.oldPassword;
      let newPassword = group.value.password;
      let repeatPassword = group.value.repeatPassword;
      
      if( ( oldPassword == '' && newPassword  == '' && repeatPassword == '' ) || 
          ( oldPassword != '' && newPassword  != '' && repeatPassword != '' ) ) {
        return null;
      }

      return {
        notEverythingFilled: true
      };      
    }
  }
}
