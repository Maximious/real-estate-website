import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstateService } from '../estate.service';
import { Estate } from '../model/estate';

@Component({
  selector: 'estate-card',
  templateUrl: './estate-card.component.html',
  styleUrls: ['./estate-card.component.css']
})
export class EstateCardComponent implements OnInit {

  @Input() estate : Estate;

  region : String;  
  image : String;
  logged : boolean = false;
  worker : boolean = false;
  admin : boolean = false;
  sameUrl : string = "";
  num : number = 0;

  constructor(
    private router: Router,
    private estateService : EstateService,
    private activatedRoute: ActivatedRoute
  ) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
    return false;
    };    
    for (let part of this.activatedRoute.url['_value']) {
      this.sameUrl = this.sameUrl + "/" + part.path;
    }    
    this.activatedRoute.queryParams.subscribe(params => {
      let num = params['estateType'];

      if(num != undefined) {
        this.num = num;
      }
    });
  }

  ngOnInit(): void {
    let log = JSON.parse(localStorage.getItem('loggedIn'));
    if(log != undefined) {
      this.logged = true;    
      if(log.type == 'admin') {
        this.admin = true;
      } else if(log.type == 'worker') {
        this.worker = true;
      }
    }   

    let add = this.estate.address.split(',');    
    this.region = add[0];
    this.image = this.selectRandomImage();
  }

  openPage(){
    if( localStorage.getItem('loggedIn') != undefined )    
    {
      localStorage.setItem('selectedEstate', this.estate._id);
      this.router.navigate(['estatePage']);
    }
  }

  selectRandomImage(){
    let availableImages = this.estate.video['img'].length;
    let selectedImage = Math.floor(Math.random() * availableImages);

    if(availableImages == 0)
    {
      return 'assets/estateDefault.png';
    }

    return this.estate.video['img'][selectedImage];
  }

  estateDetails(id){
    if(localStorage.getItem('loggedIn') != undefined)
    {
          
      localStorage.setItem('estateId',id);
      this.router.navigate(['/estate']);
    }
  }

  approveEstate(id){    
    this.estateService.approveEstate(id)
      .subscribe((answer : boolean)=>{
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([this.sameUrl],{ queryParams: { estateType: this.num } });
        })
      })
  }

  removeEstate(id){    
    this.estateService.deleteEstate(id)
      .subscribe((answer : boolean)=>{
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([this.sameUrl],{ queryParams: { estateType: this.num } });
        })
      })
  }

  promoteEstate(id, promoted){    
    this.estateService.setPromotedEstate(id,promoted)
      .subscribe((answer : boolean)=>{
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([this.sameUrl],{ queryParams: { estateType: this.num } });
        })
      })
  }
}
