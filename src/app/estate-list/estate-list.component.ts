import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstateService } from '../estate.service';
import { Estate } from '../model/estate';

@Component({
  selector: 'estate-list',
  templateUrl: './estate-list.component.html',
  styleUrls: ['./estate-list.component.css']
})
export class EstateListComponent implements OnInit {

  estates : Array<Estate>;  
  state : number = 0;  
  sameUrl : string = "";
  fileType : number;
  isAdmin : boolean = false;
  username : string;

  constructor(    
    private router: Router,
    private estateService: EstateService,
    private activatedRoute: ActivatedRoute
  ) { 
    if(localStorage.getItem('loggedIn')) {
      let user = JSON.parse(localStorage.getItem('loggedIn'));
      this.username = user.type == 'worker' ? 'agency' : user.username;
      this.isAdmin = user.type == 'admin' ? true : false;
    }
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    for (let part of this.activatedRoute.url['_value']) {
      this.sameUrl = this.sameUrl + "/" + part.path;
    }
    this.activatedRoute.queryParams.subscribe(params => {
      let num = params['estateType'];
      if( num != undefined ) {
        this.state = num;
      }
  });
  }

  ngOnInit(): void {       
    if(this.state == 0) {        
        this.estateService.getAllEstates().subscribe((estates: Array<Estate>)=>{      
          this.estates = estates;          
        })
    }
    else if(this.state == 1) {      
      this.estateService.getApprovedEstates().subscribe((estates: Array<Estate>)=>{      
        this.estates = estates;        
      })  
    } else if(this.state == 2) {      
      this.estateService.getWaitingEstates().subscribe((estates: Array<Estate>)=>{      
        this.estates = estates;        
      })
    }
    else if(this.state == 3) {
      this.estateService.getPromotedEstates().subscribe((estates: Array<Estate>)=>{      
        this.estates = estates;        
      })
    } else {      
      this.estateService.getMyEstates(this.username).subscribe((estates: Array<Estate>)=>{      
        this.estates = estates;        
      })
    }
  }   

  insertMany(files) {    
    const fileReader = new FileReader();    
    fileReader.onload = () => {      
      let data = fileReader.result as string; 
      let dataArray : Array<Estate> = [];      

      if(this.fileType == 0) {
        let sth = data.split('\n')               
        for( let line in sth ) {
          let estate = new Estate(sth[line]);
          dataArray.push(estate);                          
          }
      } else {
        dataArray = JSON.parse(data) as Array<Estate>;        
      }
      
      let dataToProcess = dataArray.length;
      let dataProcessed = 0;
      for( let currentEstate in dataArray ) {                
        this.estateService.createNewEstate(dataArray[currentEstate])
                .subscribe((answer)=>{
                  let id = answer;
                  let filesToSend = dataArray[currentEstate].video['img'].length;
                  let filesSent = 0;
                  let filesSentDone = 0;
                  while(filesSent < filesToSend)
                  {                              
                    this.estateService.addEstateImg(id, dataArray[currentEstate].video['img'][filesSent]).subscribe(answer=>{
                      filesSentDone++;
                      if(filesSentDone == filesToSend) {
                        dataProcessed++;
                        if(dataProcessed == dataToProcess) {
                          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                            this.router.navigate([this.sameUrl],{ queryParams: { usersType: this.state } });
                          })
                        }
                      }
                    })
                    filesSent++; 
                  }              
              })              
      }      
    }
    fileReader.onerror = (error) => {
      console.log(error);
    }    
    if(files[0].type == 'application/vnd.ms-excel'){
      this.fileType = 0;
    } else if(files[0].type == 'application/json') {
      this.fileType = 1;
    } else return;
    fileReader.readAsText(files[0], "UTF-8");
  }
}
