import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidator } from '../custom-validator';
import { EstateService } from '../estate.service';
import { Estate } from '../model/estate';
import { UserService } from '../user.service';

@Component({
  selector: 'app-estate-page',
  templateUrl: './estate-page.component.html',
  styleUrls: ['./estate-page.component.css']
})
export class EstatePageComponent implements OnInit {

  estate: Estate;
  region: String;
  loggedUsername : String;
  adminWorker : boolean = true;    
  newMessageModalOpened : boolean = false;

  messageForm: FormGroup;
  loading = false;
  submitted = false;

  dateRange = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  payment: FormGroup;
  price : number;

  constructor(
    private estateService : EstateService,
    private router: Router,
    private fb: FormBuilder,
    private userService : UserService,
    private customValidator : CustomValidator) { }

  ngOnInit(): void {
    this.messageForm = this.fb.group({
      receiver: ['', [Validators.required, this.customValidator.existingUser()] ],
      title: ['', [Validators.required] ],
      message:['', [Validators.required, Validators.minLength(10)]]
    });
    let id = localStorage.getItem('estateId');    
    this.estateService.getEstateById(id).subscribe((estate: Estate)=>{        
      //console.log(estate);
      this.estate = estate;
      let add = this.estate.address.split(',');    
      this.region = add[0];
      this.price = 0.2 * estate.price;
      this.messageForm.controls.receiver.setValue(estate.owner == "agency" ? "agencija" : estate.owner);
      this.messageForm.controls.title.setValue(estate.name);
    })
    this.payment = this.fb.group({
      type: ['', Validators.required]
    });
    let log = JSON.parse(localStorage.getItem('loggedIn'));
    this.loggedUsername = log.username;
    this.adminWorker = log.type != 'user';    
  }

  //getters
  get f() { return this.messageForm.controls; }  
  get fe() { return this.messageForm.errors; }

  modalClosed() {
    this.newMessageModalOpened = false;
  }

  openNewMessageModal(){
    this.newMessageModalOpened = true;
  }

  onSubmit(){
    console.log(this.f);    
    this.submitted = true;        

    // stop here if form is invalid
    if (this.messageForm.invalid) {                                        
      this.loading = false;
        return;
    }

    this.loading = true;
    
    this.userService.createNewConversation(this.loggedUsername, this.messageForm).subscribe(answer => {    
            this.loading = false;
            console.log(answer);
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate(['/estate']);
            })
      })
  }

}
