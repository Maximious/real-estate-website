import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstateSettingsComponent } from './estate-settings.component';

describe('EstateSettingsComponent', () => {
  let component: EstateSettingsComponent;
  let fixture: ComponentFixture<EstateSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstateSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstateSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
