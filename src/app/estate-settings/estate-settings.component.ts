import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { createImportSpecifier } from 'typescript';
import { EstateService } from '../estate.service';
import { Estate } from '../model/estate';

@Component({
  selector: 'app-estate-settings',
  templateUrl: './estate-settings.component.html',
  styleUrls: ['./estate-settings.component.css']
})
export class EstateSettingsComponent implements OnInit {

  estateForm: FormGroup;  
  loading = false;
  submitted = false;
  filesSent : number = 0;
  id : string;
  images = [];
  currentImage : number = 0;

  constructor(private formBuilder: FormBuilder,
    private estateService : EstateService,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    let user = JSON.parse(localStorage.getItem('loggedIn'));
    if(user == undefined) {
      this.router.navigate['/'];
    }
    this.id = localStorage.getItem('estateId');    
    this.estateForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(28)]],
      country: ['', [Validators.required]],
      city: ['', [Validators.required]],
      region: ['', [Validators.required]],
      adress: ['', [Validators.required]],
      number: ['', [Validators.required]],
      type : ['', []],
      stores : ['', [Validators.required]],
      size: ['', Validators.required],
      rooms: ['', Validators.required],
      furniture: ['', []],        
      price: ['', [Validators.required]],
      sale: ['',[]],
      img: ['',[]]                
    });        

    this.estateService.getEstateById(this.id).subscribe((estate: Estate)=>{
      let adr = estate.address.split(',');              
      this.estateForm.controls.name.setValue(estate.name);
      this.estateForm.controls.country.setValue(estate.country);
      this.estateForm.controls.city.setValue(estate.city);
      this.estateForm.controls.region.setValue(adr[0]);
      this.estateForm.controls.adress.setValue(adr[1]);
      this.estateForm.controls.number.setValue(adr[2]);
      this.estateForm.controls.type.setValue(estate.type);
      this.estateForm.controls.stores.setValue(estate.stores);
      this.estateForm.controls.size.setValue(estate.size);
      this.estateForm.controls.rooms.setValue(estate.rooms);
      this.estateForm.controls.furniture.setValue(estate.furniture);
      this.estateForm.controls.price.setValue(estate.price);
      this.estateForm.controls.sale.setValue(estate.sale);

      this.images = estate.video['img'];
    })    
  }

  //getters
  get f() { return this.estateForm.controls; }  
  get fe() { return this.estateForm.errors; }

  onSubmit() {
    this.submitted = true;        

    // stop here if form is invalid    
    if (this.estateForm.invalid) {                
        this.loading = false;
        return;
    }
        
    console.log(this.f.type);
    if( this.f.type.value == true || this.f.type.value == 'kuća' ) {
      this.f.type.setValue('kuća');
    }
    else {
      this.f.type.setValue('stan');
    }

    this.loading = true;
        
    this.estateService.updateExisting(this.estateForm, this.id)
            .subscribe((answer)=>{              

              if( this.filesSent >= this.f.img.value.length ) {
                this.router.navigate([
                  'myEstates'
                ]);
              }

              while(this.filesSent < this.f.img.value.length)
              {                            
                let fileReader = new FileReader();                
                let imageLocation = this.f.img.value[this.filesSent];
                this.filesSent++;

                fileReader.onload = (e) => {           
                  let img = fileReader.result as string;                        

                  this.estateService.addEstateImg(this.id, img).subscribe((answer)=>{
                    if( this.filesSent >= this.f.img.value.length ) {
                      this.router.navigate([
                        'myEstates'
                      ]);
                    } 
                  })                         
                }
                fileReader.readAsDataURL(imageLocation);
              }              
          })    
  }

  //imgUpload
  handleFileInput(files: FileList) {
    this.f.img.setValue(files);    
  }

  onSlide(event) {
    //console.log(event);
    let slideId = event.current.split('-');
    this.currentImage = slideId[2];
  }

  removeImage() {    
    //console.log("Removing " + this.currentImage);
    //delete this.images[this.currentImage];
    
    let url = this.router.url;

    this.estateService.removeEstateImg(this.id, this.images[this.currentImage]).subscribe((answer)=>{
      //console.log(answer);
      window.location.reload();
    })      
  }
}
