import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Estate } from './model/estate';

@Injectable({
  providedIn: 'root'
})
export class EstateService {
  
  uri = 'http://localhost:4000';

  constructor(private http: HttpClient) { }

  getAllEstates(){
    return this.http.get(`${this.uri}/estates`);
  }

  getPromotedEstates(){
    return this.http.get(`${this.uri}/estatesPromoted`);
  }

  getApprovedEstates(){
    return this.http.get(`${this.uri}/estatesApproved`);
  }

  getWaitingEstates(){
    return this.http.get(`${this.uri}/estatesUnapproved`);
  }  
  
  setPromotedEstate(id, promoted){
    let data = {
      _id : id,
      promoted : promoted
    }

    return this.http.post(`${this.uri}/setPromotedEstate`, data);
  }  

  approveEstate(id){
    let data = {
      _id : id
    }

    return this.http.post(`${this.uri}/approveEstate`, data);
  }

  deleteEstate(id){
    let data = {
      _id : id
    }
    
    return this.http.post(`${this.uri}/deleteEstate`, data);
  }

  getMyEstates(username){
    let data = {
      username : username
    }
    return this.http.post(`${this.uri}/getMyEstates`, data);
  } 

  getEstateById(_id : String){
    const data={
      "_id" : _id
    }
    return this.http.post(`${this.uri}/getEstateById`, data);
  }

  search(city, lPrice, hPrice){
    const data={
      city: city,
      lPrice: lPrice,
      hPrice: hPrice
    }
    return this.http.post(`${this.uri}/search`, data);
  }

  createNew(estateForm : FormGroup){
    let form = estateForm.controls;

    const data={
      name: form.name.value,
      approved: form.approved != undefined ? form.approved.value : false,
      country: form.country.value,
      city: form.city.value,
      region: form.region.value,
      address: form.adress.value,
      number: form.number.value,
      type: form.type.value,
      stores: form.stores.value,
      size: form.size.value,
      rooms: form.rooms.value,
      furniture: form.furniture.value,
      price: form.price.value,
      sale: form.sale.value,
      owner: form.owner.value
    }
    console.log(form);
    console.log(data);

    return this.http.post(`${this.uri}/newEstate`, data);
  } 

  createNewEstate(estate : Estate){    
    
    const data={
      name: estate.name,
      country: estate.country,
      city: estate.city,
      region: estate.address.split(',')[0],
      address: estate.address.split(',')[1],
      number: estate.address.split(',')[2],
      type: estate.type,
      stores: estate.stores,
      size: estate.size,
      rooms: estate.rooms,
      furniture: estate.furniture,
      price: estate.price,
      sale: estate.sale,
      owner: estate.owner
    }
    //console.log(form);
    //console.log(data);

    return this.http.post(`${this.uri}/newEstate`, data);
  }

  updateExisting(estateForm : FormGroup, id : String){
    let form = estateForm.controls;

    const data={
      _id : id,
      name: form.name.value,
      country: form.country.value,
      city: form.city.value,
      region: form.region.value,
      address: form.adress.value,
      number: form.number.value,
      type: form.type.value,
      stores: form.stores.value,
      size: form.size.value,
      rooms: form.rooms.value,
      furniture: form.furniture.value,
      price: form.price.value,
      sale: form.sale.value,      
    }
    //console.log(form);
    //console.log(data);

    return this.http.post(`${this.uri}/updateEstate`, data);
  } 

  addEstateImg(id, img){
    const data={
      id : id,
      img : img
    }
    
    //console.log(data);

    return this.http.post(`${this.uri}/addEstateImg`, data);
  } 

  removeEstateImg(id, img){
    const data={
      id : id,
      img : img
    }
    
    //console.log(data);

    return this.http.post(`${this.uri}/removeEstateImg`, data);
  }  

  statisticEstatePriceRange() {
    return this.http.get(`${this.uri}/statisticEstatePriceRange`);
  }

  statisticEstateCityCount() {
    return this.http.get(`${this.uri}/statisticEstateCityCount`);
  }

  statisticEstateTypeCount() {
    return this.http.get(`${this.uri}/statisticEstateTypeCount`);
  }
}
