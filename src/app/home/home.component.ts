import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstateService } from '../estate.service';
import { Estate } from '../model/estate';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit { 

  promotedEstates : Array<Estate>;
  estates : Array<Estate>;
  emptyFields : boolean;
  lowerPrice : number;
  higherPrice : number;
  city : string;

  constructor(
    private estateService : EstateService,
    private router: Router) { }

  ngOnInit(): void {
    this.estateService.getApprovedEstates().subscribe((estates: Array<Estate>)=>{
      //console.log(estates);
      this.estates = estates;
    })
    this.estateService.getPromotedEstates().subscribe((estates: Array<Estate>)=>{      
      this.promotedEstates = estates;
    })
  }  

  onSubmit(){
    if((this.city == undefined || this.city == "") &&
      (this.lowerPrice == undefined || this.lowerPrice == null) &&
      (this.higherPrice == undefined || this.higherPrice == null))
    {
      return;
    }
    this.estateService.search(this.city,this.lowerPrice,this.higherPrice)
    .subscribe((estates: Array<Estate>)=>{      
      this.estates = estates;
    })
  }

  estateDetails(id){
    if(localStorage.getItem('loggedIn') != undefined)
    {
      console.log(id);
      localStorage.setItem('estateId',id);
      this.router.navigate(['/estate']);
    }
  }
}