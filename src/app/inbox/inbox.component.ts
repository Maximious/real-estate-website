import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { COSMIC_THEME } from '@nebular/theme';
import { ConversationService } from '../conversation.service';
import { CustomValidator } from '../custom-validator';
import { UserService } from '../user.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  username : string;
  worker : boolean;
  messages : any;
  conversations : any = [];
  conversationsArchived : any = [];
  receiver : string = "";
  selectedConv : string = "";
  selectedTitle : string = "";
  isOwner : boolean = false;  
  archived : boolean = false;
  blockList : any = [];
  blocked : boolean;
  offer : boolean;
  offerPrice : number;
  newMessageModalOpened : boolean = false;
  newMessageUsername : string;
  newMessage : string;
  newMessageTitle : string;

  messageForm: FormGroup;
  loading = false;
  submitted = false;

  constructor( 
    private userService : UserService,
    private conversationService : ConversationService,
    private router : Router,
    private fb: FormBuilder,
    private customValidator : CustomValidator
     ) {
    let user = localStorage.getItem('loggedIn');
    if( user == undefined ) {
      this.router.navigate(['/']);
    }
    else {      
      let jsonFile = JSON.parse(user);      
      this.worker = jsonFile.type == 'worker';
      this.blockList = jsonFile.blockedUsers;
      this.username = jsonFile.username;
      this.userService.getUsersBlockingMe(this.username).subscribe(answer=>{
        //console.log(answer)
        for(let i in answer) {
          this.blockList.push(answer[i].username);
        }
      })
      this.userService.activeMessages(this.username).subscribe(answer => {
        //console.log(answer);
        for(let index in answer) {
          if(answer[index].archived) {
            this.conversationsArchived.push(answer[index]);
          }
          else {
            this.conversations.push(answer[index]);
          }
        }        
        if(this.conversations.length > 0)
        {
          this.offer = this.conversations[0]['offer'];
          this.offerPrice = this.conversations[0]['offerPrice'];
          this.selectedConv = this.conversations[0]['idConv'] != undefined ? this.conversations[0]['idConv'] : "";
          this.selectedTitle = this.conversations[0]['title'] != undefined ? this.conversations[0]['title'] : "";
          this.receiver = this.conversations[0]['comUser'] != undefined ? this.conversations[0]['comUser'] : "";
          this.isOwner = this.conversations[0]['owner'];        
          this.blocked = this.blockList.find(element => element == this.receiver) != undefined;
          this.conversationService.getConversationById(this.selectedConv).subscribe(answer => {
            console.log(answer);         
            this.messages = answer;                 
          })        
        }
      });      
    }     
  }

  ngOnInit(): void {
    this.messageForm = this.fb.group({
      receiver: ['', [Validators.required, this.customValidator.existingUser()] ],
      title: ['', [Validators.required] ],
      message:['', [Validators.required, Validators.minLength(10)]]
    });
  }

  //getters
  get f() { return this.messageForm.controls; }  
  get fe() { return this.messageForm.errors; }
  
  
  sendMessage(event: any, userName: string, avatar: string, reply: boolean) {
    const files = !event.files ? [] : event.files.map((file) => {
      return {
        url: file.src,
        type: file.type,
        icon: 'file-text-outline',
      };
    });
    
    this.messages.push({
      sender: this.username,
      text: event.message,
      date: new Date(),
      reply: true,
      type: files.length ? 'file' : 'text',
      files: files,
      user: {
        name: this.username
      },
    });

    this.conversationService.sendMessage(this.selectedConv, 
      true, this.username, this.receiver, event.message).subscribe(answer=>{
        console.log(answer);
      })
  }

  changeConversation(idConv, index, arc) {    
      let data = this.conversations;
      data = arc ? this.conversationsArchived : this.conversations;
      this.selectedConv = idConv;
      this.offerPrice = data[index]['offerPrice'];
      this.offer = data[index]['offer'];
      this.selectedTitle = data[index]['title'];
      this.receiver = data[index]['comUser'];
      this.isOwner = data[index]['owner'];      
      this.archived = data[index]['archived'];
      this.blocked = this.blockList.find(element => element == this.receiver) != undefined;
      this.conversationService.getConversationById(this.selectedConv).subscribe(answer => {        
        this.messages = answer;
      })
  }

  modalClosed() {
    this.newMessageModalOpened = false;
  }

  openNewMessageModal(){
    this.newMessageModalOpened = true;
  }

  onSubmit(){
    console.log(this.f);    
    this.submitted = true;        

    // stop here if form is invalid
    if (this.messageForm.invalid) {                                        
      this.loading = false;
        return;
    }

    this.loading = true;
    
    this.userService.createNewConversation(this.username, this.messageForm).subscribe(answer => {    
            this.loading = false;
            console.log(answer);
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate(['/inbox']);
            })
      })
  }

  archiveMessage() {    
    this.userService.archiveMessage(this.username, this.selectedConv).subscribe(answer => {
      //console.log(answer);
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/inbox']);
      })
    })
  }

  blockUser(){
    this.userService.blockUser(this.username, this.receiver).subscribe(answer=>{
      //console.log(answer);
      this.userService.archiveMessage(this.username, this.selectedConv).subscribe(answer=>{

      });
      let savedUser = JSON.parse(localStorage.getItem('loggedIn'));
      savedUser.blockedUsers.push(this.receiver);
      localStorage.setItem('loggedIn', JSON.stringify(savedUser));
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/inbox']);
      })
    })
  }

  unblockUser(){
    this.userService.unblockUser(this.username, this.receiver).subscribe(answer=>{
      //console.log(answer);
      this.userService.archiveMessage(this.username, this.selectedConv).subscribe(answer=>{

      });
      let savedUser = JSON.parse(localStorage.getItem('loggedIn'));
      savedUser.blockedUsers.forEach((element,index)=>{ 
          if(element==this.receiver){
            savedUser.blockedUsers.splice(index,1);
          }
      });
      localStorage.setItem('loggedIn', JSON.stringify(savedUser));
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/inbox']);
      })
    })
  }

  setOffer() {    
    if(this.offerPrice != undefined && this.offerPrice > 0)
    {
      this.userService.setOffer(this.username, this.selectedConv, this.offerPrice, this.receiver).subscribe(answer=>
        {
          //console.log(answer);
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            //this.router.navigate(['/inbox']);
          })
        })
    }
  }

  removeOffer() {
    this.userService.removeOffer(this.username, this.selectedConv, this.receiver).subscribe(answer=>
    {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(['/inbox']);
      })
    })
  }
}
