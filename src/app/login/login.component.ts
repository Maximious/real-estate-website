import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';    
import { UserService } from '../user.service';
import { User } from '../model/user'

@Component({
  selector: 'loginWindow',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    loading = false;
    submitted = false;
    error : boolean = false;
    
    constructor(
        private formBuilder: FormBuilder,  
        private router: Router,
        private userService : UserService
    ) { }

    ngOnInit() {
        this.form = this.formBuilder.group({
            username: ['', [Validators.required] ],
            password: ['', [Validators.required] ]            
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;        

        // stop here if form is invalid
        if (this.form.invalid) {                                
            console.log(this.f.password.errors);            
            return;
        }

        this.loading = true;
        
        this.userService.login(this.f.username.value, this.f.password.value)
            .subscribe((user: User)=>{
                this.loading = false;
                console.log(user);
                if(!user){
                    this.error = true;                      
                }
                else
                {
                    localStorage.setItem('loggedIn', JSON.stringify(user));
                    this.loggingComplete();
                    this.loginClosed();                 
                    this.router.navigate([user.type]);                    
                }
          })
    }

    @Output() closed = new EventEmitter<string>();
    loginClosed() {
      this.closed.emit('closedLogin');
    }

    loginClosedRegisterOpened() {        
      this.closed.emit('closedLoginOpenedRegister');
    }

    @Output() loggedIn = new EventEmitter<string>();
    loggingComplete() {
       this.loggedIn.emit('loggedIn');
    }
}
