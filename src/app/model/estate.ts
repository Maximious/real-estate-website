export class Estate{

    constructor(csvParsedLine : string) {        
        let data = csvParsedLine.split(',');
        
        this.name = data[0];
        this.country = data[1];
        this.city = data[2];
        this.address = data[3] + ", " + data[4] + ", " + data[5];
        this.type = data[6];
        this.stores = data[7];
        this.size = parseInt(data[8]);
        this.rooms = parseInt(data[9]);
        this.furniture = data[10] == 'true';
        this.sale = data[11] == 'true';
        this.price = parseInt(data[12]);
        this.owner = data[13];
        this.approved = true;
        this.promoted = false;

        this.video = {
            img : [],
            vid : []
        }
        
        let i = 14;
        while(data[i] != undefined ) {            
            this.video['img'].push("data:image/jpeg;base64," + data[i++]);
        }
    }

    _id : string;
    name : string;    
    country : string;
    city : string;
    address : string;
    type : string;    
    stores : string;
    size : number;
    rooms : number;    
    furniture : boolean;    
    video : Object;    
    sale : boolean;
    price : number;    
    owner : string;    
    approved : boolean;    
    promoted : boolean;
}