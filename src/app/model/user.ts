export class User{
    _id : string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    type: string;
    mail: string;
    country: string;
    city: string;
    status: string;
    img : string;    
    blockedUsers : Array<String>;
}