import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstateService } from '../estate.service';
import { Estate } from '../model/estate';

@Component({
  selector: 'app-my-estates',
  templateUrl: './my-estates.component.html',
  styleUrls: ['./my-estates.component.css']
})
export class MyEstatesComponent implements OnInit {

  estates : Array<Estate>;
  
  constructor(
    private estateService : EstateService,
    private router: Router) { }

  ngOnInit(): void {
    let user = JSON.parse(localStorage.getItem('loggedIn'));
    if(user.type != 'user') {
      this.router.navigate(['/' + user.type + '/estates']);
    }
    this.estateService.getMyEstates(user.username).subscribe((estates: Array<Estate>)=>{      
      this.estates = estates;
    })
  }

}
