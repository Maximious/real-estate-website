import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { CustomValidator } from '../custom-validator';

@Component({
  selector: 'registerWindow',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;  
  imageLocation : File;
  adminLogged : boolean = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,    
    private userService : UserService,
    private customValidator : CustomValidator
  ) { }

  ngOnInit() {

    if(localStorage.getItem('loggedIn') != undefined) {
      this.adminLogged = true;
    }

    this.registerForm = this.formBuilder.group({
        username: ['', [Validators.required, this.customValidator.uniqueUsername()]],        
        password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(24), 
          Validators.pattern(".*[A-Z]+.*"), Validators.pattern(".*[a-z]+.*"), 
          Validators.pattern(".*[0-9]+.*"), Validators.pattern(".*[^A-Za-z0-9]+.*"), this.customValidator.threeConsequent()]],
        repeatPassword: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],        
        mail: ['', [Validators.required, Validators.email, this.customValidator.uniqueMail()]],
        country: ['', Validators.required],
        city: ['', Validators.required],
        img: ['', []],
        type: ['', []],
        approve: [this.adminLogged, []],
    }, {validator: this.customValidator.areEqual()});        
  }

  //getters
  get f() { return this.registerForm.controls; }  
  get fe() { return this.registerForm.errors; }

  //imgUpload
  handleFileInput(files: FileList) {
    this.imageLocation = files.item(0);
    
    let fileReader = new FileReader();
    fileReader.onload = (e) => {           
      this.f.img.setValue(fileReader.result as string);      
      console.log(this.f.img);
    }    
    fileReader.readAsDataURL(this.imageLocation);
  }

  onSubmit() {
    this.submitted = true;    

    // stop here if form is invalid
    if (this.registerForm.invalid) {        
        this.loading = false;
        return;
    }

    if(this.f.type.value == true) {
      this.f.type.setValue('worker');
    }
    else {
      this.f.type.setValue('user');
    }
    this.loading = true;
    
    this.userService.register(this.registerForm)
            .subscribe((answer : boolean)=>{
                this.loading = false;
                console.log(answer);
                this.registerComplete();          
          })
  }

  @Output() regClosed = new EventEmitter<string>();
  registerClosed() {
    this.regClosed.emit('registerClosed');
  }

  @Output() registered = new EventEmitter<string>();
  registerComplete() {    
      this.registered.emit('registered');
    }  
}