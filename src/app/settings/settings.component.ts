import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EventEmitter } from 'events';
import { CustomValidator } from '../custom-validator';
import { User } from '../model/user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  username : string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,    
    private userService : UserService,
    private customValidator : CustomValidator,
    private activatedRoute: ActivatedRoute
  ) { 
    this.activatedRoute.queryParams.subscribe(params => {
      this.username = params['username'];            
    });
  }
  
  settingsForm: FormGroup;
  loading = false;
  submitted = false;  
  imageLocation : File;
  user : User;

  ngOnInit(): void {
    let username = "";
    let firstName = "";
    let lastName = ""; 
    let mail = "";
    let country = "";
    let city = "";
    let img = "";

    if( localStorage.getItem('loggedIn') != undefined ) {      
      if( this.username == undefined ) {        
        this.user = JSON.parse(localStorage.getItem('loggedIn'));
        username = this.user.username;      
        firstName = this.user.firstName;
        lastName = this.user.lastName; 
        mail = this.user.mail;
        country = this.user.country;
        city = this.user.city;
        img = this.user.img;
      }
      else {        
        this.userService.findByUsername(this.username).subscribe((user : User) => {
          console.log(user);
          this.user = user;
          this.f.username.setValue(user.username);
          this.f.firstName.setValue(user.firstName);
          this.f.lastName.setValue(user.lastName);
          this.f.mail.setValue(user.mail);
          this.f.country.setValue(user.country);
          this.f.city.setValue(user.city);
          this.f.img.setValue(user.img);          
        })
      }
    } else {
      this.router.navigate(['']);
    }

    this.settingsForm = this.formBuilder.group({
      username: [username, [Validators.required, this.customValidator.uniqueUsername()]],
      oldPassword: ['', []],
      password: ['', [Validators.minLength(8), Validators.maxLength(24), 
        Validators.pattern(".*[A-Z]+.*"), Validators.pattern(".*[a-z]+.*"), 
        Validators.pattern(".*[0-9]+.*"), Validators.pattern(".*[^A-Za-z0-9]+.*"), 
        this.customValidator.threeConsequent()]],
      repeatPassword: ['', [this.customValidator.areEqual()]],
      firstName: [firstName, Validators.required],
      lastName: [lastName, Validators.required],        
      mail: [mail, [Validators.required, Validators.email, this.customValidator.uniqueMail()]],
      country: [country, Validators.required],
      city: [city, Validators.required],
      img: [img, []],                
    }, {validator: [this.customValidator.areEqual(), this.customValidator.passwordChange()]});

  }  

  //getters
  get f() { return this.settingsForm.controls; }  
  get fe() { return this.settingsForm.errors; }

  //imgUpload
  handleFileInput(files: FileList) {
    this.imageLocation = files.item(0);
    
    let fileReader = new FileReader();
    fileReader.onload = (e) => {           
      this.f.img.setValue(fileReader.result as string);      
      console.log(this.f.img);
    }    
    fileReader.readAsDataURL(this.imageLocation);
  }

  onSubmit() {
    this.submitted = true;    
    
    if( this.f.mail.value == this.user.mail ) {
      this.f.mail.setErrors(null);      
    }
    if( this.f.username.value == this.user.username ) {
      this.f.username.setErrors(null);
    }

    let passwordChanged = true;
    if(this.f.password.value == '') {
      passwordChanged = false;    
    }
    else {
      if(this.f.oldPassword.value != this.user.password) {
          this.f.oldPassword.setErrors({
          wrongPassword : true
        })
      }
    }
    
    console.log(this.f);
    // stop here if form is invalid
    if(this.settingsForm.invalid) {        
        this.loading = false;
        return;
    }
    
    this.loading = true;
    
    this.userService.changeSettings(this.settingsForm, this.user._id, this.user.password)
            .subscribe((answer : boolean)=>{
                this.loading = false;
                if( passwordChanged )
                {
                  this.router.navigate(['logout']);
                }
                else
                {                  
                  this.user.username = this.f.username.value;
                  this.user.firstName = this.f.firstName.value;
                  this.user.lastName = this.f.lastName.value;
                  this.user.mail = this.f.mail.value;
                  this.user.country = this.f.country.value;
                  this.user.city = this.f.city.value;
                  this.user.img = this.f.img.value;

                  if( this.username == undefined ) {
                    localStorage.setItem('loggedIn', JSON.stringify(this.user));
                    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                      this.router.navigate([this.user.type + '/settings']);
                    });
                  }    
                  else {
                    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                      this.router.navigate(['/admin/users']);                    
                    });              
                  }
                }                
                //console.log(answer);                
          })        
  }
}
