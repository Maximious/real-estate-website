import {  Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { UserService } from '../user.service';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  showLoginModal : boolean = false;
  showRegisterModal : boolean = false;
  loggedIn : boolean = false;
  user : User;  

  private static userTypes : Array<String> = [
    'admin', 'user', 'worker'
  ];

  constructor(private router: Router,
    private userService : UserService) { 
      if( localStorage.getItem('loggedIn') != undefined )    
      {
          this.user = JSON.parse(localStorage.getItem('loggedIn'));
          this.loggedIn = true;
          let url = this.router.url.split("/")[1];    
          if(( ToolbarComponent.userTypes.includes(url) && url != this.user.type  ) || 
            (url == "" && this.user.type != 'user'))
          {      
            this.router.navigate([this.user.type]);
          }                    
      }   
      else
      {
        this.router.navigate(['/']);
      }
    }

  ngOnInit(): void {            
  }

  loginModalVisible() : void {
    this.showLoginModal = true;
  }     
  
  closeWindow(event) {
    if(event === "closedLogin")
    {
      this.showLoginModal = false;
    }
    else if(event === "registerClosed")
    {
      this.showRegisterModal = false;
    }
    else if(event === "closedLoginOpenedRegister")
    {
      this.showLoginModal = false;
      this.showRegisterModal = true;
    }
  }

  registerComplete(event) {
    if(event === "registered")
    {
      this.showRegisterModal = false;
    }
  }

  logged(event) {
    if(event === "loggedIn")
    {
      this.loggedIn = true;
    }    
  }

  settings(){
    this.router.navigate(['/settings']);
  }

  home(){
    this.router.navigate(['/']);
  }

  myEstates(){
    this.router.navigate(['/myEstates']);
  }  
}
