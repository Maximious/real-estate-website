import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../model/user';
import { UserService } from '../user.service';

@Component({
  selector: 'user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {

  @Input() user : User;  
  sameUrl : string = "";
  num : number = 0;

  constructor(
    private router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };        
    for (let part of this.activatedRoute.url['_value']) {
      this.sameUrl = this.sameUrl + "/" + part.path;
    }    
    this.activatedRoute.queryParams.subscribe(params => {
      let num = params['usersType'];

      if(num != undefined) {
        this.num = num;
      }
    });
  }

  ngOnInit(): void {    
    //console.log(this.user);    
  }  

  approveUser(selectedUserUsername) {        
    this.userService.approveUser(selectedUserUsername)
      .subscribe((answer : boolean)=>{
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([this.sameUrl],{ queryParams: { usersType: this.num } });
        })
      })    
  }

  removeUser(selectedUserUsername) {    
    this.userService.deleteUser(selectedUserUsername)
      .subscribe((answer : boolean)=>{
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([this.sameUrl],{ queryParams: { usersType: this.num } });
        })
      })
  }  
}
