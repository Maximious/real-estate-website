import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../model/user';
import { UserService } from '../user.service';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users : Array<User>;
  showRegisterModal : boolean = false;
  state : number = 0;  
  sameUrl : string = "";
  
  constructor(
    private router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };            
    for (let part of this.activatedRoute.url['_value']) {
      this.sameUrl = this.sameUrl + "/" + part.path;
    }    
    this.activatedRoute.queryParams.subscribe(params => {
      let num = params['usersType'];
      if( num != undefined ) {
        this.state = num;
      }
  });
  }

  ngOnInit(): void {       
    //console.log(this.state);
    if(this.state == 0) {
        //console.log("first");
        this.userService.getAllUsers().subscribe((users: Array<User>)=>{      
          this.users = users;          
        })
    }
    else if(this.state == 1) {
      //console.log("second");
      this.userService.getApprovedUsers().subscribe((users: Array<User>)=>{      
        this.users = users;
        console.log("second");
      })  
    } else {
      //console.log("third");
      this.userService.getWaitingUsersUsers().subscribe((users: Array<User>)=>{      
        this.users = users;        
      })
    }      
  }  

  registerComplete(event) {    
    this.showRegisterModal = false;
    console.log(this.sameUrl);
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([this.sameUrl],{ queryParams: { usersType: this.state } });
    })  
  }

  registerCanceled(event) {    
    this.showRegisterModal = false;    
  }

  openRegister() { 
    this.showRegisterModal = true;
  }
}
