import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  uri = 'http://localhost:4000';

  constructor(private http: HttpClient) { }

  login(username, password){
    const data={
      username: username,
      password: password
    }
    return this.http.post(`${this.uri}/login`, data);
  }  

  register(registerForm : FormGroup){
    let form = registerForm.controls;
    const data={
      username: form.username.value,
      password: form.password.value,
      firstName: form.firstName.value,
      lastName: form.lastName.value,
      mail: form.mail.value,
      country: form.country.value,
      city: form.city.value,
      img: form.img.value,
      status: form.approve.value ? 'approved' : 'waiting',
      type: form.type.value,
    }
    return this.http.post(`${this.uri}/register`, data);
  }  

  changeSettings(registerForm : FormGroup, id : String, oldPassword : String){
    let form = registerForm.controls;
    const data={
      _id : id,
      username: form.username.value,
      password: form.password.value != '' ? form.password.value : oldPassword,
      firstName: form.firstName.value,
      lastName: form.lastName.value,
      mail: form.mail.value,
      country: form.country.value,
      city: form.city.value,
      img: form.img.value
    }
    return this.http.post(`${this.uri}/changeSettings`, data);
  }

  checkUniqueUsername(username){
    const data={
      username: username
    }
    return this.http.post(`${this.uri}/checkUsernameTaken`, data);
  }

  checkUniqueMail(mail){
    const data={
      mail : mail
    }
    return this.http.post(`${this.uri}/checkMailTaken`, data);
  }

  getAllUsers(){
    return this.http.get(`${this.uri}/users`);
  }

  getApprovedUsers(){
    return this.http.get(`${this.uri}/approvedUsers`);
  }

  getWaitingUsersUsers(){
    return this.http.get(`${this.uri}/waitingUsers`);
  }   

  findByUsername(username){
    return this.http.get(`${this.uri}/findByUsername?username=${username}`);
  }

  approveUser(username){
    return this.http.get(`${this.uri}/approveUser?username=${username}`);
  }

  deleteUser(username){
    return this.http.get(`${this.uri}/removeUser?username=${username}`);
  }

  activeMessages(username){
    const data={
      username : username
    }
    return this.http.post(`${this.uri}/activeMessages`, data);
  }




  chatParticipants(username){
    return this.http.get(`${this.uri}/chatParticipants?username=${username}`);
  }

  getMessageHistory(fromUser, toUser){
    return this.http.get(`${this.uri}/getMessageHistory?fromUser=${fromUser}&toUser=${toUser}`);
  }

  sendMessage(fromUser, toUser, message){
    const data={
      fromUser: fromUser,
      toUser: toUser,
      message : message
    }
    return this.http.post(`${this.uri}/sendMessage`, data);
  } 

  getUsersBlockingMe(username){
    let data = {
      username : username
    }
    return this.http.post(`${this.uri}/getUsersBlockingMe`, data);
  }

  createNewConversation( username, messageForm : FormGroup ) {
    let form = messageForm.controls;
    let data = {
      username : username,
      title : form.title.value, 
      comUser : form.receiver.value,
      text : form.message.value
    }
    return this.http.post(`${this.uri}/createNewConversation`, data);
  }

  archiveMessage( username, idConv ) {    
    let data = {
      username : username,      
      idConv : idConv  
    }
    return this.http.post(`${this.uri}/archiveMessage`, data);
  }

  blockUser( username, blockingUser ) {            
    let data = {
      username : username,      
      blockingUser : blockingUser  
    }
    return this.http.post(`${this.uri}/blockUser`, data);
  }

  unblockUser( username, blockingUser ) {    
    let data = {
      username : username,      
      blockingUser : blockingUser  
    }
    return this.http.post(`${this.uri}/unblockUser`, data);
  }

  setOffer( username, idConv, offerPrice, comUser ) {    
    let data = {
      username : username,      
      idConv : idConv,
      offerPrice : offerPrice,
      comUser : comUser
    }
    console.log(data);
    return this.http.post(`${this.uri}/setOffer`, data);
  }

  removeOffer( username, idConv, comUser ) {    
    let data = {
      username : username,      
      idConv : idConv,
      comUser : comUser
    }
    console.log(data);
    return this.http.post(`${this.uri}/removeOffer`, data);
  }
}
